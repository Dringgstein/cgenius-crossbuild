#!/bin/bash
if [ -d "Commander-Genius" ] 
then
    cd Commander-Genius
    git pull
    cd .. 
else
    git clone --depth 1 https://github.com/gerstrong/Commander-Genius.git
fi
export CI_PROJECT_DIR=$(pwd)/Commander-Genius
mkdir -p /tmp && cd "$_"
mkdir -p $CI_PROJECT_DIR/output
cmake -DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/toolchains/toolchain-mingw.cmake -DUSE_BOOST=0 -DPULL_DLLS=1 -DAPPEND_SHA=1 -DSDL2_MIXER_INCLUDE_DIR=/usr/x86_64-w64-mingw32/include/SDL2 -DSDL2_INCLUDE_DIR=/usr/x86_64-w64-mingw32/include/SDL2 $CI_PROJECT_DIR
cmake --build . 
cpack -G NSIS
cp *.exe $CI_PROJECT_DIR/output

