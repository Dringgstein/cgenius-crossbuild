# CGenius-crossbuild

Bunch of docker files which help build Commander Genius for all the possible platforms automatically using Docker.

## Build docker on System

The system is split into core, which is the base and then the appropiate docker file for the target system. It basically is the latest Debian system with some basic build tools.

Whenever you use one of the other docker files for building Commander Genius you get the crossbuild toolchain with SDL2, SDL2image, SDL2mixer, SDL2ttf and some other dependencies preinstalled.

### Build docker on System

#### Create the image

Build your core image:

```
cd core
docker build -t cgenius/crossbuild:core  .
cd ..
```

For a different systems just replace __core__  by __win__, __winnsis, __mac__, __linux__ or __android__ respectively.

### Build docker for windows systems


#### Build Commander Genius using the Windows docker 

For tests:
```
cd win
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:win echo helloworld
```

If you want to build it
```
cd win
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:win  ./buildAll.sh
cd ..
```

If you want to enter the container using an interactive bash

```
docker run --rm -v $(pwd):/workdir -i -t cgenius/crossbuild:win /bin/bash
```

#### Build Commander Genius using the MacOS docker

For tests:
```
cd mac
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:mac echo helloworld
```

If you want to build Commander Genius
```
cd mac
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:mac  ./buildAll.sh
cd ..
```

If you want to enter the container using an interactive bash

```
docker run --rm -v $(pwd):/workdir -i -t cgenius/crossbuild:mac /bin/bash
```


#### Build Commander Genius using the Linux docker

For tests:
```
cd linux
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:linux echo helloworld
```

If you want to build Commander Genius
```
cd linux
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:linux  ./buildAll.sh
cd ..
```

If you want to enter the container using an interactive bash

```
docker run --rm -v $(pwd):/workdir -i -t cgenius/crossbuild:linux /bin/bash
```
 
 
 
#### Build Commander Genius using the Android docker

For tests:
```
cd android
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:android echo helloworld
```

If you want to build Commander Genius
```
cd android
docker run --rm -v $(pwd):/workdir cgenius/crossbuild:android  ./buildAll.sh
cd ..
```

If you want to enter the container using an interactive bash

```
docker run --rm -v $(pwd):/workdir -i -t cgenius/crossbuild:android /bin/bash
```
 
