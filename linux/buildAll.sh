#!/bin/bash
if [ -d "Commander-Genius" ] 
then
    cd Commander-Genius
    git pull
    cd .. 
else
    git clone --depth 1 https://github.com/gerstrong/Commander-Genius.git
fi
export CI_PROJECT_DIR=$(pwd)/Commander-Genius
mkdir -p /tmp && cd "$_"
mkdir -p $CI_PROJECT_DIR/output
cmake -DUSE_BOOST=0 -DAPPEND_SHA=1 $CI_PROJECT_DIR -DCREATE_DEBS=1
cmake --build . 
cpack -G TGZ
#cpack -G DEB
cp *.tar.gz $CI_PROJECT_DIR/output
#cp *.deb $CI_PROJECT_DIR/output

