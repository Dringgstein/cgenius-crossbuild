#!/bin/bash
if [ -d "Commander-Genius" ] 
then
    cd Commander-Genius
    git checkout .
    git pull
    git submodule update -r -i
    cd .. 
else
    git clone --depth 1 https://github.com/gerstrong/Commander-Genius.git
    cd Commander-Genius
    git submodule update --init --recursive
    cd ..
fi

rm Commander-Genius/src/engine/xxd.c


# For curl+openssl extra repo
export HOST_TAG=linux-x86_64

export MIN_SDK_VERSION=25
export ANDROID_HOME=/tmp
export NDK=$ANDROID_HOME/ndk/25.2.9519653
export ANDROID_NDK_HOME=$NDK

# Some path exports so we reach everything easily
export PATH=$PATH:$ANDROID_HOME/cmdline-tools
export PATH=$PATH:$ANDROID_NDK_HOME
export PATH=$PATH:/tmp/build-tools/34.0.0

# Helps building archiving openssl
pushd $NDK/toolchains/llvm/prebuilt/linux-x86_64/bin/
ln -s llvm-ar aarch64-linux-android21-ar
ln -s llvm-ar armv7a-linux-androideabi16-ar
ln -s llvm-ar x86_64-linux-android21-ar
ln -s llvm-ar i686-linux-android16-ar
popd

# Pull libsdlandroid.
git clone https://github.com/gerstrong/sdlandroid.git

cd sdlandroid

git submodule update --init project/jni/application/commandergenius/commandergenius

if [ ! -d "/workdir/commandergenius/project/licenses" ] 
then
    ln -s $ANDROID_HOME/licenses /workdir/sdlandroid/project
fi

git submodule update --init project/jni/application/commandergenius/commandergenius project/jni/sdl2 project/jni/sdl2_image project/jni/sdl2_mixer project/jni/sdl2_ttf

pushd project/jni/application/commandergenius/commandergenius/src
git submodule update --init 
# We already have xxd. Don't build it when sdl-android parses the sources.
rm engine/xxd.c
popd

pushd project/jni/sdl2_image/external/
git submodule update --init --recursive
popd

pushd project/jni/sdl2_mixer/external/
git submodule update --init --recursive
popd



if [ -L "project/jni/application/src" ] 
then 
    rm project/jni/application/src 
else
    echo "Something went wrong while trying to symlink CommanderGenius for sdlandroid correctly"
    exit 1
fi

ln -s commandergenius project/jni/application/src
#ln -s /tmp/sdl2ttfdev /workdir/sdlandroid/project/jni/sdl2_ttf

pushd src/commandergenius/src/engine
xxd -i gamelauncher.menu gamelauncher.menu.h
popd

./changeAppSettings.sh -a
./build.sh
